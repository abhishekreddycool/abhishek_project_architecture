#pragma once
#include "Component.h"
class Projectile : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Projectile, Component)

private:
	sf::Vector2f mousePosition;
	sf::Vector2f projectilePosition;
	sf::Vector2f fireDirection;
	GameObject* player = nullptr;

protected:
	void initialize() override;

public:
	float projectileSpeed = 500.0f;	//Default projectile speed if its not assigned in the level file
	Projectile() = default;
	~Projectile();
	void update(float deltaTime) override;
	void load(json::JSON& node) override;
	void onTriggerEnter(const Collision* const collisionData) override;
	void onCollisionEnter(const Collision* const collisionData) override;
	void setEnabled(bool _enabled) override;
};

