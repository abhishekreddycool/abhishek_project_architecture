#include "GameCore.h"
#include "GameEngine.h"
#include "ProjectEngine.h"
#include "Player.h"
#include "Projectile.h"
#include "SpawnFactory.h"
#include "Enemy.h"
#include "Item.h"
#include "FinishLogic.h"

void registerGameClasses()
{
	REGISTER_DYNAMIC_CLASS(Player);
	REGISTER_DYNAMIC_CLASS(Projectile);
	REGISTER_DYNAMIC_CLASS(SpawnFactory);
	REGISTER_DYNAMIC_CLASS(Enemy);
	REGISTER_DYNAMIC_CLASS(Item);
	REGISTER_DYNAMIC_CLASS(FinishLogic);

}

int main(int argc, char* argv[])
{
    registerGameClasses();

    GameEngine::instance().initialize(&ProjectEngine::instance());
	GameEngine::instance().gameLoop();

	return -1;
}