#include "GameCore.h"
#include "SpawnFactory.h"
#include "PrefabAsset.h"
#include "ICollidable.h"

IMPLEMENT_DYNAMIC_CLASS(SpawnFactory)

void SpawnFactory::initialize()
{
	if (!isEnabled())
	{
		return;
	}
	Component::initialize();
}


SpawnFactory::~SpawnFactory()
{
}

void SpawnFactory::update(float deltaTime)
{
	timer += deltaTime;
	if (timer >= enemySpawnTime)
	{
		PrefabAsset* enemyPrefab = (PrefabAsset*)AssetManager::instance().GetAssetByGUID(enemyGuid);

		if (enemyPrefab != nullptr)
		{
			GameObject* enemyGo = GameObjectManager::instance().instantiatePrefab(enemyPrefab->getID());
			enemyGo->getTransform()->setPosition(getGameObject()->getTransform()->getPosition());
		}
		timer = 0;
	}
}

void SpawnFactory::load(json::JSON& node)
{
	if (node.hasKey("enemyGuid"))
	{
		enemyGuid = node["enemyGuid"].ToString();
	}

	if (node.hasKey("itemGuid"))
	{
		itemGuid = node["itemGuid"].ToString();
	}


	if (node.hasKey("enemySpawnTime"))
	{
		enemySpawnTime = node["enemySpawnTime"].ToFloat();
	}

}

void SpawnFactory::onTriggerEnter(const Collision* const collisionData)
{
	int otherColliderIx = 1;
	if (collisionData->colliders[otherColliderIx]->getGameObject() == getGameObject())
	{
		otherColliderIx = 0;
	}

	if (collisionData->colliders[otherColliderIx]->getGameObject()->getComponent("Projectile") != nullptr)
	{
		GameObjectManager::instance().removeGameObject(getGameObject());
		GameObjectManager::instance().removeGameObject(collisionData->colliders[otherColliderIx]->getGameObject());

		PrefabAsset* itemPrefab = (PrefabAsset*)AssetManager::instance().GetAssetByGUID(itemGuid);

		if (itemPrefab != nullptr)
		{
			GameObject* itemGo = GameObjectManager::instance().instantiatePrefab(itemPrefab->getID());
			itemGo->getTransform()->setPosition(getGameObject()->getTransform()->getPosition());
		}
	}
}

void SpawnFactory::onCollisionEnter(const Collision* const collisionData)
{
}

void SpawnFactory::setEnabled(bool _enabled)
{
	enabled = _enabled;
	if (enabled && getGameObject()->isEnabled() && !initialized)
	{
		initialize();
	}
}
