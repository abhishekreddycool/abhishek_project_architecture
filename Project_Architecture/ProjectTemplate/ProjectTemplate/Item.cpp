#include "GameCore.h"
#include "Item.h"
#include "ICollidable.h"

IMPLEMENT_DYNAMIC_CLASS(Item)

void Item::initialize()
{
	if (!isEnabled())
	{
		return;
	}
	Component::initialize();

	std::string playerstring = "Player";
	player = GameObjectManager::instance().getGameObjectWithComponent(playerstring);

}

Item::~Item()
{
}

void Item::update(float deltaTime)
{
}

void Item::load(json::JSON& node)
{
}

void Item::onTriggerEnter(const Collision* const collisionData)
{
	int otherColliderIx = 1;
	if (collisionData->colliders[otherColliderIx]->getGameObject() == getGameObject())
	{
		otherColliderIx = 0;
	}
	if (collisionData->colliders[otherColliderIx]->getGameObject() == player)
	{
		GameObjectManager::instance().removeGameObject(getGameObject());	
	}
}

void Item::onCollisionEnter(const Collision* const collisionData)
{
}

void Item::setEnabled(bool _enabled)
{
	enabled = _enabled;
	if (enabled && getGameObject()->isEnabled() && !initialized)
	{
		initialize();
	}
}
